#!/bin/bash

set -e
set -u

default_make="make"
default_makeflags="-j$(nproc)"

MAKE="${MAKE:-$default_make}"
MAKEFLAGS="${MAKEFLAGS:-$default_makeflags}"

export CC="/usr/lib/ccache/gcc"
export CXX="/usr/lib/ccache/g++"

DESTDIR="$PWD/fakeroot/opt/riscv"

rm -rf $DESTDIR
mkdir -p $DESTDIR

cd riscv-gnu-toolchain
./configure --prefix=$DESTDIR
$MAKE $MAKEFLAGS
