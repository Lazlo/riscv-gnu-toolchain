#!/bin/bash

set -e
set -u

pkgs=""
pkgs="$pkgs autoconf"
pkgs="$pkgs automake"
pkgs="$pkgs autotools-dev"
pkgs="$pkgs curl"
pkgs="$pkgs python3"
pkgs="$pkgs libmpc-dev"
pkgs="$pkgs libmpfr-dev"
pkgs="$pkgs libgmp-dev"
pkgs="$pkgs gawk"
pkgs="$pkgs build-essential"
pkgs="$pkgs bison"
pkgs="$pkgs flex"
pkgs="$pkgs texinfo"
pkgs="$pkgs gperf"
pkgs="$pkgs libtool"
pkgs="$pkgs patchutils"
pkgs="$pkgs bc"
pkgs="$pkgs zlib1g-dev"
pkgs="$pkgs libexpat-dev"

apt-get -q install -y $pkgs
