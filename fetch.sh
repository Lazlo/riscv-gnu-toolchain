#!/bin/bash

d=riscv-gnu-toolchain
if [ -d $d ]; then
	exit 0
fi
git clone --depth 1 https://github.com/riscv/riscv-gnu-toolchain.git
cd riscv-gnu-toolchain
git submodule update --init --recursive
